from tomcat:8

maintainer Eike Friedrich

copy target/*.war /usr/local/tomcat/webapps

expose 8080  

CMD ["catalina.sh", "run"]

